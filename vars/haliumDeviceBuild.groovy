def call(String haliumVersion, String deviceName, String lunchName, boolean combinedBootImage = false, boolean mkaDtboImage = true, boolean mkaVbmetaImage = true, boolean mkaVendorImage = false) {
  String agentTag = "halium-$haliumVersion"
  String artifacts = 'halium*'

  pipeline {
    agent { label "$agentTag" }
    options {
      skipDefaultCheckout()
      buildDiscarder(logRotator(artifactNumToKeepStr: '30'))
      throttle(["$agentTag"])
    }
    triggers {
      cron('@daily')
    }
    stages {
      stage('Clean before building') {
        steps {
          deleteDir()
        }
      }
      stage('Build port') {
        steps {
          checkout scm
          dir ('lastArtifacts') {
            script {
              copyArtifacts(projectName: "${JOB_NAME}",
                            selector: lastSuccessful(),
                            optional: true)
            }
          }
          sh 'mv ./lastArtifacts/used-repos.txt last-used-repos.txt || true'
          dir ('scripts') {
            git(branch: 'main',
                url: 'https://gitlab.com/ubports/community-ports/jenkins-ci/halium-build-tools.git')
            sh """
              nice ./clone.sh '$deviceName' '$haliumVersion'
              if ./check-diff.sh '$WORKSPACE' '/opt/halium_build/$haliumVersion'; then
                echo 'check-diff detected the same build as last time, no need to build any more.'
                mv $WORKSPACE/lastArtifacts/$artifacts '$WORKSPACE'
                exit 0
              fi
              nice ./build.sh '$deviceName' '$lunchName' '$haliumVersion' '$combinedBootImage' '$mkaDtboImage' '$mkaVbmetaImage' '$mkaVendorImage'
              nice ./build-tarball.sh '$deviceName' '$WORKSPACE' '$haliumVersion' '$combinedBootImage'
            """
          }
        }
      }
    }
    post {
      always {
        archiveArtifacts(artifacts: 'last-used-repos.txt,used-repos.txt',
                         fingerprint: true,
                         onlyIfSuccessful: false,
                         allowEmptyArchive: true)
        archiveArtifacts(artifacts: artifacts, fingerprint: true, onlyIfSuccessful: true, allowEmptyArchive: false)
      }
      success {
        script {
          if (currentBuild?.getPreviousBuild()?.resultIsWorseOrEqualTo("UNSTABLE")) {
            notifyTelegram("Halium build for device $deviceName: FIXED")
          }
        }
      }
      unstable {
        notifyTelegram("Halium build for device $deviceName: UNSTABLE, check ${JOB_URL}")
      }
      failure {
        script {
          if (currentBuild?.getPreviousBuild()?.resultIsWorseOrEqualTo("FAILURE")) {
            notifyTelegram("Halium build for device $deviceName: NOT FIXED, check ${JOB_URL}")
          } else {
            notifyTelegram("Halium build for device $deviceName: FAILURE, check ${JOB_URL}")
          }
        }
      }
      cleanup {
        deleteDir()
      }
    }
  }
}

def notifyTelegram(String message) {
  withCredentials([usernamePassword(credentialsId: 'a25d8b20-4a81-43e9-ac37-dcfb5285790a', usernameVariable: 'TELEGRAM_BOT_CREDS_USR', passwordVariable: 'TELEGRAM_BOT_CREDS_PWD')]) {
    env['TELEGRAM_BOT_MSG'] = message
    sh('curl -s -X POST https://api.telegram.org/$TELEGRAM_BOT_CREDS_PWD/sendMessage -d chat_id=$TELEGRAM_BOT_CREDS_USR -d text="$TELEGRAM_BOT_MSG"')
  }
}
