#!/bin/sh -xe
device="$1"
ver="$2"

cd "/opt/halium_build/$ver"

rm -f ./.repo/local_manifests/*

repo version

repo sync -v -c --force-sync --force-remove-dirty
./halium/devices/setup "$device" -v --force-sync --force-remove-dirty
